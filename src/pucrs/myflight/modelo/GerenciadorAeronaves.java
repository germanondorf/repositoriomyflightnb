package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorAeronaves {
    private ArrayList<Aeronave> aeronaves;

    public GerenciadorAeronaves(){
        aeronaves = new ArrayList<>();
    }

    public void adicionar(Aeronave cia) {
        aeronaves.add(cia);
    }

    public Aeronave buscarCodigo(String cod) {
        for (Aeronave cia: aeronaves)
            if (cod.equals(cia.getCodigo()))
                return cia;
        // Não encontrou, retorna null
        return null;
    }


    public ArrayList<Aeronave> listarTodas() {
        ArrayList<Aeronave> nova = new ArrayList<>();
        for(Aeronave cia: aeronaves)
            nova.add(cia);
        return nova;
    }
}
