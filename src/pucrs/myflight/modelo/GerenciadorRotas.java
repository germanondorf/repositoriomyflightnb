package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorRotas {

    private ArrayList<Rota> rotas;

    public GerenciadorRotas() {
        rotas = new ArrayList<>();
    }

    public void adicionar(Rota cia) {
        rotas.add(cia);
    }

    public Rota buscarOrigem(Aeroporto cod) {
        for (Rota cia: rotas)
            if (cod.equals(cia.getOrigem()))
                return cia;
        // Não encontrou, retorna null
        return null;
    }


    public ArrayList<Rota> listarTodas() {
        ArrayList<Rota> nova = new ArrayList<>();
        for(Rota cia: rotas)
            nova.add(cia);
        return nova;
    }
}
