package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorAeroportos {
    private ArrayList<Aeroporto> aeroportos;

    public GerenciadorAeroportos() {
        aeroportos = new ArrayList<>();
    }

    public void adicionar(Aeroporto cia) {
        aeroportos.add(cia);
    }

    public Aeroporto buscarCodigo(String cod) {
        for (Aeroporto cia: aeroportos)
            if (cod.equals(cia.getCodigo()))
                return cia;
        // Não encontrou, retorna null
        return null;
    }

    public ArrayList<Aeroporto> listarTodas() {
        ArrayList<Aeroporto> nova = new ArrayList<>();
        for(Aeroporto cia: aeroportos)
            nova.add(cia);
        return nova;
    }

}
