package pucrs.myflight.modelo;
import java.time.Duration;
import java.time.LocalDateTime;

public class App {

	public static void main(String[] args) {
		//CIAAEREA
		CiaAerea cia1 = new CiaAerea("JJ","LATAM LINHAS AEREAS");
		CiaAerea cia2 = new CiaAerea("G3","GOL LINHAS AEREAS SA");
		CiaAerea cia3 = new CiaAerea("TP","TAP PORTUGAL");
		CiaAerea cia4 = new CiaAerea("AD","AZUL LINHAS AEREAS");
        CiaAerea cia5 = new CiaAerea("AR","AEROLINAS ARGENTINAS");

        GerenciadorCias ger = new GerenciadorCias();
		ger.adicionar(cia1);
		ger.adicionar(cia2);
		ger.adicionar(cia3);
		ger.adicionar(cia4);
        ger.adicionar(cia5);

		System.out.print(ger.listarTodas());
		System.out.println("busca cia por codigo:   " + ger.buscarCodigo("JJ"));
		System.out.println("busca cia por nome:     " + ger.buscarNome("AZUL LINHAS AEREAS"));

		System.out.println("-----/-----");
		//AERONAVES
		Aeronave a1 = new Aeronave("733","BOEING 737-300",140);
		Aeronave a2 = new Aeronave("73G","BOEING 737-700",126);
		Aeronave a3 = new Aeronave("380","AIRBUS INDUSTRIE A380",644);
		Aeronave a4 = new Aeronave("764","BOEING 767-400",304);
		Aeronave a5 = new Aeronave("738","BOEING 737-800",160);
		Aeronave a6 = new Aeronave("332","AIRBUS A330-200",234);
		Aeronave a7 = new Aeronave("320","BOEING 767-430",300);

		GerenciadorAeronaves gerA = new GerenciadorAeronaves();
		gerA.adicionar(a1);
		gerA.adicionar(a2);
		gerA.adicionar(a3);
		gerA.adicionar(a4);
		gerA.adicionar(a5);
		gerA.adicionar(a6);
		gerA.adicionar(a7);

		System.out.print(gerA.listarTodas());
		System.out.println("busca aeronave por codigo:  " + gerA.buscarCodigo("73G"));

		System.out.println("-----/-----");
		//AEROPORTOS
		Geo g1 = new Geo(-29.9939,-51.1711);
		Geo g2 = new Geo(-23.4356,-46.4731);
		Geo g3 = new Geo(38.7742,-9.1342);
		Geo g4 = new Geo(25.7933,-80.2906);
		Geo g5 = new Geo(-22.8134,-43.2494);
        Geo g6 = new Geo(-34.559200,-58.415600);
        Aeroporto aeroporto1 = new Aeroporto("POA","SALGADO FILHO INTL APT",g1);
		Aeroporto aeroporto2 = new Aeroporto("GRU","SAO PAULO GUARULHOS INTL APT",g2);
		Aeroporto aeroporto3 = new Aeroporto("LIS","LISBON",g3);
		Aeroporto aeroporto4 = new Aeroporto("MIA","MIAMI INTERNATIONAL APT",g4);
		Aeroporto aeroporto5 = new Aeroporto("GIG"," ANTONIO CARLOS JOBIM",g5);
        Aeroporto aeroporto6 = new Aeroporto("AEP"," JORGE NEWBERY",g6);


		GerenciadorAeroportos gerenciadorAeroportos = new GerenciadorAeroportos();
		gerenciadorAeroportos.adicionar(aeroporto1);
		gerenciadorAeroportos.adicionar(aeroporto2);
		gerenciadorAeroportos.adicionar(aeroporto3);
		gerenciadorAeroportos.adicionar(aeroporto4);
        gerenciadorAeroportos.adicionar(aeroporto5);
        gerenciadorAeroportos.adicionar(aeroporto6);

		System.out.println(gerenciadorAeroportos.listarTodas());
		System.out.println("busca aeroporto por codigo:   " + gerenciadorAeroportos.buscarCodigo("LIS"));

		System.out.println("-----/-----");
		//ROTAS
		Rota rota1 = new Rota(cia2,aeroporto2,aeroporto1,a5);
		Rota rota2 = new Rota(cia2,aeroporto1,aeroporto2,a5);
		Rota rota3 = new Rota(cia3,aeroporto4,aeroporto3,a6);
		Rota rota4 = new Rota(cia1,aeroporto3,aeroporto5,a7);
        Rota rota5 = new Rota(cia2,aeroporto1,aeroporto5,a1);
        Rota rota6 = new Rota(cia5,aeroporto1,aeroporto6,a2);



        GerenciadorRotas gerR = new GerenciadorRotas();
		gerR.adicionar(rota1);
		gerR.adicionar(rota2);
		gerR.adicionar(rota3);
		gerR.adicionar(rota4);
        gerR.adicionar(rota5);
        gerR.adicionar(rota6);

        System.out.println(gerR.listarTodas());
		System.out.println("busca rota por origem:   " + gerR.buscarOrigem(aeroporto1));

        System.out.println("-----/-----");
        //VOOS

        Voo voo1 = new Voo (rota2,  LocalDateTime.of(2016,8,10,8,0),Duration.ofMinutes(90));
        Voo voo2 = new Voo (rota5,  LocalDateTime.of(2016,8,10,15,0),Duration.ofMinutes(120));
        Voo voo3 = new Voo (rota6,  LocalDateTime.of(2016,8,15,12,0),Duration.ofMinutes(120));

        voo1.setStatus(Voo.Status.ATRASADO);
        voo3.setStatus(Voo.Status.CANCELADO);

        GerenciadorVoos gerenciadorVoos = new GerenciadorVoos();
        gerenciadorVoos.adicionar(voo1);
        gerenciadorVoos.adicionar(voo2);
        gerenciadorVoos.adicionar(voo3);

        System.out.println(gerenciadorVoos.listarTodas());
        System.out.println("busca voo por data: " + gerenciadorVoos.buscarData(LocalDateTime.of(2016,8,10,8,0)));

		System.out.println("-----/-----");
		//CONSULTAS DA TAREFA
		/*Listar os dados de todos os vôos cuja origem é determinado aeroporto, a partir do seu código.
		Mostrar a geolocalização de todos os aeroportos que operam vôos em um determinado período do dia
		(hora e minuto inicial, hora e minuto final - consulte a documentação em java.time.LocalDateTime).*/

		System.out.println("busca por destino: "+ gerenciadorVoos.buscaListaDestino("GRU"));
		System.out.println("busca por data de comeco e fim:	" + gerenciadorVoos.buscaListaLocalizacao(LocalDateTime.of(2016,1,1,0,0),
				LocalDateTime.of(2016,8,20,0,0)));



	}
}
