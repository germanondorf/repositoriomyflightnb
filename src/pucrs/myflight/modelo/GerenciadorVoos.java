package pucrs.myflight.modelo;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class GerenciadorVoos {
    private ArrayList<Voo> voos;

    public GerenciadorVoos() {
        voos = new ArrayList<>();
    }

    public void adicionar(Voo cia) {
        voos.add(cia);
    }

    public Voo buscarData(LocalDateTime cod) {
        for (Voo cia: voos)
            if (cod.equals(cia.getDatahora()))
                return cia;
        // Não encontrou, retorna null
        return null;
    }

    public ArrayList<Voo> buscaListaDestino(String cod){
        ArrayList<Voo> nova = new ArrayList<>();
        for (Voo cia: voos) {
            if (cod.equals(cia.getRota().getDestino().getCodigo())) {
                //return cia;
                nova.add(cia);
            }
        }
        return nova;
    }


    public ArrayList<Aeroporto> buscaListaLocalizacao(LocalDateTime cod, LocalDateTime cod2 ) {
        ArrayList<Aeroporto> nova = new ArrayList<>();
        for (Voo cia : voos) {
            if (cia.getDatahora().isAfter(cod) && cia.getDatahora().isBefore(cod2)) {
                nova.add(cia.getRota().getOrigem());
                nova.add(cia.getRota().getDestino());
            }
        } // Não encontrou, retorna null
        return nova;
    }

    public ArrayList<Voo> listarTodas() {
        ArrayList<Voo> nova = new ArrayList<>();
        for(Voo cia: voos)
            nova.add(cia);
        return nova;
    }

}
